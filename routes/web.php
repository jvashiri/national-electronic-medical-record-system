<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController; 


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome');});

Route::get('/home', 'App\Http\Controllers\HomeController@redirect')->middleware('verified');
Route::get('/dashboard', 'App\Http\Controllers\HomeController@adminDashboard')->name('admin.dashboard');
Route::get('/user/dashboard', 'App\Http\Controllers\HomeController@userDashboard');//->name('dashboard');




/*Route::get('/dashboard/admin', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
Route::get('/dashboar', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');*/

require __DIR__.'/auth.php';
